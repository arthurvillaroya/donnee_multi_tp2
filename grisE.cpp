// test_couleur.cpp : Seuille une image en niveau de gris
#include <iostream>
#include <stdio.h>
#include "image_ppm.h"

int pixMin(OCTET image[], int x, int y);

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

  for (int i = 1; i < nH-1; i++){
    for (int j = 1; j < nW-1; j++){
        ImgOut[i*nW+j]=pixMin(ImgIn, i, j);
    }
  }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}

int pixMin(OCTET image[], int x, int y){
  int mini = 255;
  for(int dx = -1; dx < 2; dx++){
    for(int dy = -1; dy < 2; dy++){
      if(image[(x+dx)*256 + (y+dy)] < mini){
        mini = image[(x+dx)*256 + (y+dy)];
      }
    }
  }

  return mini;
}